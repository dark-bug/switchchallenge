from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Payment(models.Model):
    payment_id = models.BigAutoField(primary_key=True, null=False, unique=True) 
    amount = models.FloatField(null=False)
    PaymentChoices = models.TextChoices('PaymentChoice', 'mbway credit_card')
    payment_method = models.CharField(null=False, choices=PaymentChoices.choices, max_length=50)
    created_at = models.DateTimeField(null=False, auto_now_add=True)
    StatusChoices = models.TextChoices('StatusChoice', 'success error settled')
    status = models.CharField(choices=StatusChoices.choices, default="success", max_length=20)
    settled_at = models.DateTimeField(null=True)
    settled_amount = models.FloatField(null=True)
    #card
    number = models.CharField(null=True, max_length=30)
    name = models.CharField(null=True, max_length=100)
    expiration_month = models.IntegerField(null=True, validators=[MinValueValidator(1), MaxValueValidator(12)])
    expiration_year = models.IntegerField(null=True, validators=[MinValueValidator(2020)])
    cvv = models.PositiveSmallIntegerField(null=True)
    #mbway
    phone_number = models.CharField(null=True, max_length=30)