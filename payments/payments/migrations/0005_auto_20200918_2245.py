# Generated by Django 3.1.1 on 2020-09-18 22:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0004_auto_20200918_2244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='status',
            field=models.CharField(blank=True, choices=[('success', 'Success'), ('error', 'Error'), ('settled', 'Settled')], max_length=20),
        ),
    ]
