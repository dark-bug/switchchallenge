# Generated by Django 3.1.1 on 2020-09-20 20:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0004_auto_20200920_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='status',
            field=models.CharField(choices=[('success', 'Success'), ('error', 'Error'), ('settled', 'Settled')], default='success', max_length=20),
        ),
    ]
