from django.http import HttpResponse
from django.views import View
from payments.models import Payment
from django.core import serializers

import json
import datetime
import pytz
import time

class GeneralPaymentsView(View):

    #get all payments
    def get(self, request, *args, **kwargs):
        payment = Payment()
        payments = Payment.objects.only("payment_id", "amount", "payment_method", "created_at", "status", "settled_at", "settled_amount").all()
        payments_serialized = serializers.serialize("json", list(payments))

        return HttpResponse(payments_serialized, status=200)

    #create payment
    def post(self, request, *args, **kwargs):
        payment = Payment()
        body = json.loads(request.body)

        if "amount" in body:
            payment.amount = body["amount"]
        if "payment_method" in body:
            payment.payment_method = body["payment_method"]

        if "payment_method" in body:
            if body["payment_method"] == "credit_card":
                try:
                    payment.number = body["number"]
                    payment.name = body["name"]
                    payment.expiration_month = body["expiration_month"]
                    payment.expiration_year = body["expiration_year"]
                    payment.cvv = body["cvv"]
                    payment.status = "success"
                except:
                    return HttpResponse("Internal server error", status=500)
            else:
                payment.phone_number = body["phone_number"]
            payment.save()

        return HttpResponse(request, status=200)

class SpecificPaymentOperationsView(View):
    #get payment details
    def get(self, request, *args, **kwargs):
        payment = Payment()
        payment_id_in_url = kwargs["id"]

        try:
            
            specific_payment = Payment.objects.filter(pk=payment_id_in_url).all()
            payments = serializers.serialize("json", specific_payment)
        except:
            return HttpResponse("Exception while getting the payment details", status=500)
        
        return HttpResponse(payments, status=200)

    #mark payment as settled
    def put(self, request, *args, **kwargs):
        payment = Payment()
        body = json.loads(request.body)

        payment_id_in_url = kwargs["id"]

        try:
            specific_payment = Payment.objects.get(payment_id=payment_id_in_url)
            if specific_payment.status != "settled":
                specific_payment.settled_at = datetime.datetime.now()
                specific_payment.settled_amount = body["settled_amount"]
                specific_payment.status = "settled"
                specific_payment.save()
            else:
                return HttpResponse("Given payment already settled")
        except: 
            return HttpResponse("Error while settling the payment", status=500)

        return HttpResponse("Mark payment as settled", status=200)

    #delete payment
    def delete(self, request, id):
        payment = Payment()

        payment_id_in_url = id
        try:
            specific_payment = Payment.objects.get(payment_id=payment_id_in_url)
            specific_payment.delete()
        except:
            return HttpResponse("Exception on delete of payment with id " + str(payment_id_in_url), status=500)

        return HttpResponse("Deleted payment with id "+str(payment_id_in_url), status=200)

class SearchPaymentsView(View):
    def post(self, request, *args, **kwargs):
        payment = Payment()
        body = json.loads(request.body)
        filters = {}

        if "payment_id" in body:
            filters["payment_id"] = body["payment_id"]

        #amount 
        if "amount" in body:
            amount_range = body["amount"]
            different_amounts = amount_range.split(" TO ")
            filters["amount__range"] = (different_amounts[0], different_amounts[1])

        if "payment_method" in body :
            filters["payment_method"] = body["payment_method"]
        
        if "status" in body:
            filters["status"] = body["status"]

        #created_at
        if "created_at" in body:
            date_range = body["created_at"]
            split_date = date_range.split(" TO ")
            param_start_date = time.strptime(split_date[0], "YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]")
            param_end_date = time.strptime(split_date[1], "YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]")

            filters["created_at__range"] = (param_start_date, param_end_date)

        #settled_at
        if "settled_at" in body:
            date_range = body["settled_at"]
            split_date = date_range.split(" TO ")

            filters["settled_at__range"] = (split_date[0], split_date[1])    
    
        try:
            search_results = Payment.objects.filter(**filters).all()
            search_results_serialized = serializers.serialize("json", list(search_results))
            return HttpResponse(search_results_serialized, status=200)
        except Exception as e:
            return HttpResponse("Internal server error" + str(e), status=500)


#search
#https://docs.djangoproject.com/en/3.1/topics/db/queries/#retrieving-specific-objects-with-filters