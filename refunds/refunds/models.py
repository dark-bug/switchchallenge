from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Refund(models.Model):
    refund_id = models.BigAutoField(primary_key=True, null=False, unique=True)
    payment_id = models.CharField(null=False, max_length=200)
    created_at = models.DateTimeField(null=False, auto_now_add=True)
    amount = models.FloatField(null=True)