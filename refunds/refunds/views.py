from django.http import HttpResponse
from django.views import View
from refunds.models import Refund
from django.core import serializers
from django.db.models import Sum

import requests
import json

class GeneralRefundsView(View):
    #get all refunds
    def get(self, request, *args, **kwargs):
        refund = Refund()
        refunds = Refund.objects.all()
        refunds_serialized = serializers.serialize("json", list(refunds))

        return HttpResponse(refunds_serialized, status=200)

    def post(self, request, *args, **kwargs):
        refund = Refund()
        body = json.loads(request.body)

        if "payment_id" in body and "amount" in body:
            payment_id = body["payment_id"]
            
            #get the given payment_id details
            #payment_details = requests.get('http://webpayments:1111/api/payments/'+str(payment_id)).json()
            try:
                payment_details = requests.get('http://webpayments:1111/api/payments/'+str(payment_id)).json()
            except: 
                return HttpResponse("ID not recognized by the payments API", status=400)

            #check if payment id exists
            if payment_details != []:
                #get all the refunds with the given payment id
                refunds_for_given_payment_id = Refund.objects.filter(payment_id=payment_id).all().aggregate(Sum("amount"))
            
                if refunds_for_given_payment_id["amount__sum"] != None:
                    new_sum_value = int(refunds_for_given_payment_id["amount__sum"]) + int(body["amount"])
                
                    if new_sum_value <= payment_details[0]["fields"]["amount"]:
                        refund.payment_id = payment_id
                        refund.amount = body["amount"]
                        refund.save()
                    else:
                        return HttpResponse("Refund exceeds the limit", status=400)       
                else:   
                    refund.payment_id = payment_id
                    refund.amount = body["amount"]
                    refund.save()
                return HttpResponse(payment_details, status=200)
            else:
                #payment_id doens't exist
                return HttpResponse("Invalid payment id", status=400)
            
        else:
            return HttpResponse("Invalid input", status=400)     

class SpecificRefundOperations(View):
    #get refund details
    def get(self, request, *args, **kwargs):
        refund = Refund()
        refund_id_in_url = kwargs["id"]

        try:
            specific_refund = Refund.objects.filter(refund_id=refund_id_in_url).all()
            refund_details = serializers.serialize("json", specific_refund)
        except:
            return HttpResponse("Exception while getting the refund details", status=500)
        
        return HttpResponse(refund_details, status=200)  

class SearchRefundsView(View):
    def post(self, request, *args, **kwargs):
        refund = Refund()
        body = json.loads(request.body)
        filters = {}

        if "refund_id" in body:
            filters["refund_id"] = body["refund_id"]
        
        if "payment_id" in body:
            filters["payment_id"] = body["payment_id"]

        try:
            search_results = Refund.objects.filter(**filters).all()
            search_results_serialized = serializers.serialize("json", list(search_results))
            return HttpResponse(search_results_serialized, status=200)
        except Exception as e:
            return HttpResponse("Internal server error" + str(e), status=500)