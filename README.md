# Switch Challenge - Payments and refunds #

This README documents how to run the Switch Challenge by Daniel Amaral.

### How to run the codebase? ###

* Clone the repository
* In the command-line, under /switchchallenge,  execute
```
$ docker-compose up
```
 After this, both the refunds and payments databases should be up, as well as both the microservices for refunds and payments.

### Payments API Endpoints ###

#### API pattern: /api/payments

* List all the payments
    * GET localhost:1111/api/payments
* Get the details of a specific payment
    * GET /api/payments/[:id]
* Search payments
    * POST localhost:1111/api/payments/search
    * Body example:
        ```json
            {
                "amount": "10 TO 145",
                "status": "success",
                "payment_method": "mbway"
            }
        ```
* Create new payments 
    * POST localhost:1111/api/payments
    * Body example:
        ```json
            {
                "amount": 10, 
                "method": "credit_card", 
                "number" :"4111111111111111", 
                "name" : "John Doe", 
                "expiration_month" : "10", 
                "expiration_year" : "2042", "cvv" : "123"
            }
        ```
* Mark payment as settled 
    * PUT localhost:1111/api/payments/[:id]
    * Body example:
        ```json
            {
                "settled_amount": 5
            }
        ```

* Delete payment 
    * DELETE localhost:1111/api/payments/[:id]

### Refunds API Endpoints ###

* API pattern: /api/refunds

* List all the refunds
    * GET localhost:2222/api/refunds
* Get the details of a specific refund
    * GET /api/refund/[:id]
* Search refunds
    * POST localhost:2222/api/refunds/search
    * Body example:
        ```json
            {
                "refund_id": 4,
                "payment_id": 55
            }
        ```
* Create new refunds 
    * POST localhost:2222/api/refunds
    * Body example:
        ```json
            {
                "amount": 10, 
                "payment_id": 5
            }
        ```


